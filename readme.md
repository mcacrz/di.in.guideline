# Guideline

Life is constant change. To continue to thrive as a business on the spaceship earth we must look ahead, understand the trends and forces that will shape our future and move swiftly to prepare for what's to come. We must get ready today for the day after tomorrow. It creates a long-term destination for our business and provides us with a guideline to inform our decisions.


## Our Mission

Our Roadmap starts with our mission. It declares our purpose as a company and serves as the standard against which we weigh our actions and decisions.


**We combine your passion, excellence and knowledge with ours to build softwares that helps you perform better towards your purpose.**


## Our Vision

Our vision serves as the framework for our Roadmap and guides every aspect of our business by describing what we need to accomplish in order to continue achieving sustainable, quality growth.

- People: Be a great place to work where people are inspired to be the best they can be.
- Projects: Bring to the world a portfolio of great products that anticipate and satisfy people's desires and needs - and make as many lives better as possible.
- Clients: Nurture a winning network of clients, together we create mutual, enduring value.
- Planet: Be a responsible citizen that makes a difference by helping build and support sustainable communities.
- Profit: Maximize long-term return to shareowners while being mindful of our overall responsibilities.
- Productivity: Be a highly effective, lean and fast-moving organization.


## Culture

Our culture defines the attitudes and behaviors that will be required of us to make our vision a reality.

### Live Our Values

Our values serve as a compass for our actions and describe how we behave in the world.

- Leadership: The courage to shape a better future
- Collaboration: Leverage collective genius
- Integrity: Be real
- Accountability: If it is to be, it's up to me
- Passion: Committed in heart and mind
- Diversity: As inclusive as our team
- Quality: What we do, we do to the best of our possibilities without compromising

### Focus on the Market

Focus on needs of our customers
Get out into the market and listen, observe and learn
Possess a world view
Focus on execution in the marketplace every day
Be insatiably curious

### Work Smart

Act with urgency
Remain responsive to change
Have the courage to change course when needed
Remain constructively discontent
Work efficiently

### Act Like Owners

Be accountable for our actions and inactions
Steward system assets and focus on building value
Reward our people for taking risks and finding better ways to solve problems
Learn from our outcomes -- what worked and what didn’t


## Be a delodian

Inspire creativity, passion, optimism and fun
